package de.justdev.jcloud.console;

import java.util.HashMap;
import java.util.Map;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 31.07.2016.
 *
 * @author JustDev [Justus]
 */
public class CommandHandling {
    private final Map<String, CommandExecutor> commands = new HashMap<>();

    public void registerExecutor(String command, CommandExecutor executor) {
        if (commands.containsKey(command)) {
            commands.remove(command);
        }
        commands.put(command, executor);
    }

    boolean dispatchCommand(String[] args) {
        if (commands.containsKey(args[0])) {
            String[] argsAfterCommand = new String[args.length - 1];
            for (int i = 0; i < args.length; i++) {
                if (i == 0) continue;
                argsAfterCommand[i - 1] = args[i];
            }
            commands.get(args[0]).onCommand(argsAfterCommand);
            return true;
        }
        return false;
    }
}
