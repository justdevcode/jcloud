package de.justdev.jcloud.clouddaemon.bootstrap;

import de.justdev.jcloud.clouddaemon.bootstrap.io.handler.AuthenticationHandler;
import de.justdev.jcloud.clouddaemon.bootstrap.io.handler.ToplayerPacketHandler;
import de.justdev.jcloud.clouddaemon.ui.JCloudDaemon;
import de.justdev.jcloud.clouddaemon.util.OSValidator;
import de.justdev.jcloud.console.CommandHandling;
import de.justdev.jcloud.console.Console;
import de.justdev.jcloud.maintenance.util.Dumper;
import de.justdev.jcloud.protocol.io.handler.LengthBasedDecoder;
import de.justdev.jcloud.protocol.io.handler.LengthBasedEncoder;
import de.justdev.jcloud.protocol.io.handler.PacketDecoder;
import de.justdev.jcloud.protocol.io.handler.PacketEncoder;
import de.justdev.jcloud.protocol.packets.RegisterPacket;
import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;


/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 31.07.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class JCloudDaemonLauncher {
    //general master connection properties
    /* CONSTANCES */
    public static final int PORT = 22220;
    public static final int SUBPORT = 25569;
    public static final String HOST = "localhost";


    public static Channel masterChannel;
    //console client
    public final Console console = new Console(new CommandHandling());
    private final ChannelGroup subServers = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    //local netty stuff [master]
    private Bootstrap clientBootstrap;
    private ChannelFuture masterConnection;
    private EventLoopGroup masterWorkerGroup;
    //local netty stuff [subserver]
    private ServerBootstrap bootstrap;
    private ChannelFuture hostChannelfuture;
    private EventLoopGroup workerGroup;
    private EventLoopGroup bossGroup;

    public JCloudDaemonLauncher() {
        Runtime.getRuntime().traceMethodCalls(true);
        //set instance in api
        JCloudDaemon.setInstance(new JCloudManager(this));
        if (new OSValidator().validate()) {
            JCloudDaemon.getInstance().initUsageWatcher();
        }

        new Thread() {
            @Override
            public void run() {
                prepareMasterConnection();
            }
        }.start();
        new Thread() {
            @Override
            public void run() {
                prepareSubConnections();
            }
        }.start();
        //register basic commands
        testCommands();
        //start console reading
        console.read();
    }

    public void prepareMasterConnection() {
        /* MASTER CONNECTION */
        //init worker
        masterWorkerGroup = new NioEventLoopGroup();

        try {
            //Bootstrap
            clientBootstrap = new Bootstrap();
            clientBootstrap.group(masterWorkerGroup);
            clientBootstrap.channel(NioSocketChannel.class);
            clientBootstrap.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    log.log(Level.DEBUG, "Channel initialised, preparing pipeline");
                    //prepare masterChannel pipeline
                    ChannelPipeline pipeline = socketChannel.pipeline();
                    pipeline.addLast("framedecoder", new LengthBasedDecoder());
                    pipeline.addLast("frameencoder", new LengthBasedEncoder());
                    pipeline.addLast("encoder", new PacketEncoder());
                    pipeline.addLast("decoder", new PacketDecoder());
                    pipeline.addLast("handler", new ToplayerPacketHandler());
                    //set socketchannel (masterconnection) for closing
                    masterChannel = socketChannel;
                }
            });

            //connect to server
            log.log(Level.INFO, "Client verbindet auf " + HOST + ":" + PORT);
            masterConnection = clientBootstrap.connect(HOST, PORT).sync();

            //authentication on masterserver
            masterChannel.writeAndFlush(new RegisterPacket(-1, "daemon", ""));

            //start console
            log.log(Level.INFO, "Connected! Type 'help' for help or any command to proceed");

            masterChannel.closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            //shutdown worker
            masterWorkerGroup.shutdownGracefully();
        }
    }

    public void prepareSubConnections() {
        /* SUBSERVER CONNECTION */
        //init event loop groups
        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();

        try {
            //bootstrap
            bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup);
            bootstrap.channel(NioServerSocketChannel.class);

            //prepare pipeline on channel init
            bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    log.log(Level.DEBUG, "Subserver verbunden!");
                    //prepare masterChannel pipeline
                    ChannelPipeline pipeline = socketChannel.pipeline();
                    pipeline.addLast("framedecoder", new LengthBasedDecoder());
                    pipeline.addLast("frameencoder", new LengthBasedEncoder());
                    pipeline.addLast("encoder", new PacketEncoder());
                    pipeline.addLast("decoder", new PacketDecoder());
                    pipeline.addLast("authhandler", new AuthenticationHandler());
                    //add masterChannel to masterChannel list
                    subServers.add(socketChannel);
                }
            });

            bootstrap.option(ChannelOption.SO_BACKLOG, 50);
            bootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);

            //bind server to port
            hostChannelfuture = bootstrap.bind(34343).sync();
            log.log(Level.INFO, "Lowerlayerserver auf Port " + SUBPORT + " gestartet");
            hostChannelfuture.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (hostChannelfuture != null) {
                try {
                    hostChannelfuture.channel().close().sync();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //shutdown event loop groups
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }

    }

    private void testCommands() {
        //Register testCommands command
        console.getCommandHandling().registerExecutor("dump", args -> {
            log.log(Level.INFO, "Creating dumps");
            Dumper.dump();
            return;
        });

        console.getCommandHandling().registerExecutor("exit", args -> {
            shutdown();
            return;
        });
    }

    public void shutdown() {
        log.log(Level.INFO, "Shutting down daemon!");
        log.log(Level.TRACE, "Closing master connection");
        try {
            //close masterChannel
            masterConnection.channel().close().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            //shutdown worker
            masterWorkerGroup.shutdownGracefully();
        }
        log.log(Level.TRACE, "Closing sub connections");
        while (subServers.iterator().hasNext()) {
            try {
                subServers.iterator().next().close().sync();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            //close host channel
            log.log(Level.TRACE, "Closing host channel");
            hostChannelfuture.channel().close().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            //shutdown event loop groups
            log.log(Level.TRACE, "Closing event loop groups");
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
        log.log(Level.TRACE, "Closing console");
        console.stopReading();
        log.log(Level.INFO, "Daemon exited");
        System.exit(0);
    }

    public static void main(String[] args) {
        new JCloudDaemonLauncher();
    }
}
