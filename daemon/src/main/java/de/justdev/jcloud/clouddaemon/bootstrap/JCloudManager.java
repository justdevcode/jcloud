package de.justdev.jcloud.clouddaemon.bootstrap;

import de.justdev.jcloud.clouddaemon.cloudserver.CloudServer;
import de.justdev.jcloud.clouddaemon.cloudserver.ServerManager;
import de.justdev.jcloud.clouddaemon.template.BukkitTemplateManager;
import de.justdev.jcloud.clouddaemon.template.BungeeTemplateManager;
import de.justdev.jcloud.clouddaemon.ui.JCloudDaemon;
import de.justdev.jcloud.clouddaemon.util.FileUtils;
import de.justdev.jcloud.protocol.packets.Packet;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.io.File;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 01.08.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class JCloudManager extends JCloudDaemon {
    private final JCloudDaemonLauncher launcher;

    private final ServerManager serverManager;
    private final BukkitTemplateManager bukkitTemplateManager;
    private final BungeeTemplateManager bungeeTemplateManager;

    public JCloudManager(JCloudDaemonLauncher launcher) {
        this.launcher = launcher;
        this.serverManager = new ServerManager();
        this.bukkitTemplateManager = new BukkitTemplateManager();
        this.bungeeTemplateManager = new BungeeTemplateManager();

        log.log(Level.INFO, "Starting usage watcher");

        //remove server dir where some instances may left behind
        log.log(Level.DEBUG, "Removing serverdir sucess: " + FileUtils.removeServerDir());
        //create new serverdir
        log.log(Level.DEBUG, "Creating new serverdir sucess: " + new File("server").mkdirs());
    }

    @Override
    public ServerManager getServerManager() {
        return serverManager;
    }

    @Override
    public BukkitTemplateManager getBukkitTemplateManager() {
        return bukkitTemplateManager;
    }

    @Override
    public BungeeTemplateManager getBungeeTemplateManager() {
        return bungeeTemplateManager;
    }

    @Override
    public void initUsageWatcher() {
        new UsageWatcher().start();
    }

    @Override
    public boolean pushMaster(Packet packet) {
        if (JCloudDaemonLauncher.masterChannel == null) {
            return false;
        }
        JCloudDaemonLauncher.masterChannel.writeAndFlush(packet);
        return true;
    }

    @Override
    public void shutdown() {
        for (CloudServer cloudServer : serverManager.getCloudserver()) {
            cloudServer.stop(true);
        }
        launcher.shutdown();
    }
}
