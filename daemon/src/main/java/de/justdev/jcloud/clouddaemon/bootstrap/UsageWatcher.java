package de.justdev.jcloud.clouddaemon.bootstrap;

import de.justdev.jcloud.clouddaemon.ui.JCloudDaemon;
import de.justdev.jcloud.protocol.packets.LogEntryPacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.DaemonInfoPacket;
import de.justdev.jcloud.util.Watcher;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.util.Map;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 09.08.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class UsageWatcher extends Thread {

    @Override
    public void run() {
        log.log(Level.TRACE, "Getting server data");
        Map<String, Integer> data = Watcher.getUsageData();

        if (data == null) {
            log.log(Level.WARN, "Cannot recieve serverdata");
            JCloudDaemon.getInstance().pushMaster(new LogEntryPacket("Cannot access server usages! Please check logs"));
            return;
        }

        if (!JCloudDaemon.getInstance().pushMaster(new DaemonInfoPacket(
                data.get("memory_total") != null ? data.get("memory_total") : -1,
                data.get("memory_used") != null ? data.get("memory_used") : -1,
                data.get("swap_total") != null ? data.get("swap_total") : -1,
                data.get("swap_used") != null ? data.get("swap_used") : -1,
                data.get("jvm_cpu_usage") != null ? data.get("jvm_cpu_usage") : -1,
                data.get("cpu_used") != null ? data.get("cpu_used") : -1))) {
            log.log(Level.INFO, "Cannot send usage! Connection is not established");
        }

        //Wait one minute
        try {
            Thread.sleep(1000 * 20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //rerun
        run();
    }
}
