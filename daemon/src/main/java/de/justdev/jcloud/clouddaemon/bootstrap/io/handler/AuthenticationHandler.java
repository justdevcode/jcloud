package de.justdev.jcloud.clouddaemon.bootstrap.io.handler;

import com.google.common.base.Preconditions;
import de.justdev.jcloud.clouddaemon.cloudserver.CloudServer;
import de.justdev.jcloud.clouddaemon.ui.JCloudDaemon;
import de.justdev.jcloud.protocol.packets.RegisterPacket;
import io.netty.channel.ChannelHandlerContext;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 05.08.2016.
 *
 * @author JustDev [Justus]
 */
public class AuthenticationHandler extends de.justdev.jcloud.protocol.io.handler.AuthenticationHandler {
    @Override
    public boolean valiedAuth(RegisterPacket registerPacket, ChannelHandlerContext channelHandlerContext) {
        //todo implement  authmanager with key validation (registerPacket#getKey)
        return true;
    }

    @Override
    public void onSuccess(RegisterPacket packet, ChannelHandlerContext ctx) {
        //add essential packethandler when auth is completed successfully
        CloudServer targetServer = JCloudDaemon.getInstance().getServerManager().getCloudServer(packet.getId());
        Preconditions.checkNotNull(targetServer, "Cloudserverinstance must exists");
        targetServer.setCtx(ctx);
        ctx.pipeline().addAfter("decoder", "handler", new LowerlayerPacketHandler(targetServer));
    }
}
