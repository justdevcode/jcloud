package de.justdev.jcloud.clouddaemon.bootstrap.io.handler;

import de.justdev.jcloud.clouddaemon.cloudserver.CloudServer;
import de.justdev.jcloud.clouddaemon.ui.JCloudDaemon;
import de.justdev.jcloud.protocol.packets.LogEntryPacket;
import de.justdev.jcloud.protocol.packets.Packet;
import de.justdev.jcloud.protocol.packets.PacketTypes;
import de.justdev.jcloud.protocol.packets.lowerlayerprotocol.StatePacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.ForwardEntryPacket;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 05.08.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class LowerlayerPacketHandler extends de.justdev.jcloud.protocol.io.handler.PacketHandler {
    private final CloudServer handlingServer;

    public LowerlayerPacketHandler(CloudServer handlingServer) {
        this.handlingServer = handlingServer;
    }

    @Override
    protected void onChannelRead(ChannelHandlerContext ctx, Packet packet, PacketTypes type) {
        switch (type) {
             /*
             * LOWERLAYER
             */

            case LOWERPLAYER_STATESERVER: {
                StatePacket packet1 = ((StatePacket) packet);
                JCloudDaemon.getInstance().pushMaster(
                        new de.justdev.jcloud.protocol.packets.toplayerprotocol.StatePacket(handlingServer.getId(),
                                packet1.getPlayerCount(), packet1.getState()));
                break;
            }

            case LOG: {
                LogEntryPacket packet1 = ((LogEntryPacket) packet);
                JCloudDaemon.getInstance().pushMaster(new ForwardEntryPacket(handlingServer.getId(), packet1.getLogEntry()));
                break;
            }

            case REGISTER_AUTH: {
                log.log(Level.INFO, "Client tries to authenticate twice");
                break;
            }

            default: {
                log.log(Level.WARN, "Unhandled packet: " + packet.getClass());
                break;
            }
        }
    }
}