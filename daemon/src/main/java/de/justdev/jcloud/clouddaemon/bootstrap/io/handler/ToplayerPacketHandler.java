package de.justdev.jcloud.clouddaemon.bootstrap.io.handler;

import de.justdev.jcloud.clouddaemon.cloudserver.CloudServer;
import de.justdev.jcloud.clouddaemon.ui.JCloudDaemon;
import de.justdev.jcloud.protocol.packets.Packet;
import de.justdev.jcloud.protocol.packets.PacketTypes;
import de.justdev.jcloud.protocol.packets.ShutdownPacket;
import de.justdev.jcloud.protocol.packets.YouWereDisconnectedPacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.CommandPacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.CreateBungeePacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.CreateServerPacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.RemoveBungeePacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.RemoveServerPacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.ShutdownServerPacket;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 31.07.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class ToplayerPacketHandler extends de.justdev.jcloud.protocol.io.handler.PacketHandler {

    @Override
    protected void onChannelRead(ChannelHandlerContext ctx, Packet packet, PacketTypes type) {
        switch (type) {
            /*
             * TOPLAYER
             */
            case TOPLAYER_CREATEBUNGEE: {
                CreateBungeePacket packet1 = ((CreateBungeePacket) packet);
                JCloudDaemon.getInstance().getServerManager().createNewBungeeServer(packet1.getId(), packet1.getRam(), packet1.getTemplate());
                break;
            }

            case TOPLAYER_REMOVEBUNGEE: {
                RemoveBungeePacket packet1 = ((RemoveBungeePacket) packet);
                JCloudDaemon.getInstance().getServerManager().removeBungeeServer(packet1.getId());
                break;
            }

            case TOPLAYER_CREATESERVER: {
                CreateServerPacket packet1 = ((CreateServerPacket) packet);
                JCloudDaemon.getInstance().getServerManager().creaetNewBukkitServer(packet1.getId(), packet1.isConsole(),
                        packet1.isCracked(), packet1.getCraftbukkit(), packet1.getWord(), packet1.getPlugins(), packet1.getRam());
                break;
            }

            case TOPLAYER_REMOVESERVER: {
                RemoveServerPacket packet1 = ((RemoveServerPacket) packet);
                CloudServer cloudServer = JCloudDaemon.getInstance().getServerManager().getBukkitCloudServer(packet1.getId());
                if (cloudServer == null) {
                    log.log(Level.WARN, "Trying to remove invalied bukkit server");
                    throw new IllegalStateException("Cloudserver " + packet1.getId() + " cannot be found!");
                } else {
                    JCloudDaemon.getInstance().getServerManager().removeBukkitServer(packet1.getId());
                }
                break;
            }

            case TOPLAYER_SHUTDOWNSERVER: {
                ShutdownServerPacket packet1 = ((ShutdownServerPacket) packet);
                CloudServer cloudServer = JCloudDaemon.getInstance().getServerManager().getBukkitCloudServer(packet1.getId());
                if (cloudServer == null) {
                    log.log(Level.WARN, "Trying to stop invalied bukkit server");
                    throw new IllegalStateException("Cloudserver " + packet1.getId() + " cannot be found!");
                } else {
                    cloudServer.stop(false);
                }
                break;
            }

            case REGISTER_AUTH: {
                log.log(Level.INFO, "Client tries to authenticate twice");
                break;
            }

            case DISCONNECT: {
                YouWereDisconnectedPacket packet1 = ((YouWereDisconnectedPacket) packet);
                log.log(Level.ERROR, "Daemon was disconnected! (by YouWereDisconnectedPacket)");
                log.log(Level.ERROR, "Reason: " + packet1.getReason());
                throw new IllegalStateException("Daemon was disconnected!");
            }

            case SHUTDOWN: {
                ShutdownPacket packet1 = ((ShutdownPacket) packet);
                log.log(Level.INFO, "Shutting down daemon! " + packet1.getMessage());
                JCloudDaemon.getInstance().shutdown();
                break;
            }

            case COMMAND: {
                CommandPacket packet1 = ((CommandPacket) packet);
                CloudServer cloudServer = JCloudDaemon.getInstance().getServerManager().getCloudServer(packet1.getId());
                if (cloudServer == null) {
                    log.log(Level.WARN, "Trying to stop invalied bukkit server");
                    throw new IllegalStateException("Cloudserver " + packet1.getId() + " cannot be found!");
                } else {
                    cloudServer.stop(false);
                }
                cloudServer.getCtx().writeAndFlush(new de.justdev.jcloud.protocol.packets.CommandPacket(packet1.getCommandAndArgs()));
                break;
            }

            default: {
                log.log(Level.WARN, "Unhandled packet: " + packet.getClass());
                break;
            }
        }
    }
}