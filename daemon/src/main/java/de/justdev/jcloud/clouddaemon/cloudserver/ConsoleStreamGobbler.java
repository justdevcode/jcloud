package de.justdev.jcloud.clouddaemon.cloudserver;

import de.justdev.jcloud.clouddaemon.util.StreamGobbler;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.io.InputStream;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 04.08.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class ConsoleStreamGobbler extends StreamGobbler {
    private final String[] lines;

    public ConsoleStreamGobbler(InputStream is, int linelimit) {
        super(is);
        lines = new String[linelimit];
        setConsumer(this::newLine);
    }

    private void newLine(String s) {
        log.log(Level.DEBUG, s);
        System.arraycopy(lines, 0, lines, 1, lines.length - 2);
        lines[0] = s;
    }
}
