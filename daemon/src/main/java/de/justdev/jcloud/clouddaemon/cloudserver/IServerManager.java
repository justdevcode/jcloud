package de.justdev.jcloud.clouddaemon.cloudserver;

import de.justdev.jcloud.clouddaemon.cloudserver.specific.BukkitCloudServer;
import de.justdev.jcloud.clouddaemon.cloudserver.specific.BungeeCloudServer;

import java.util.List;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 05.08.2016.
 *
 * @author JustDev [Justus]
 */
public interface IServerManager {
    void creaetNewBukkitServer(int id, boolean console, boolean cracked, String craftbukkit, String world, String plugin, int ram);

    void removeBukkitServer(int id);

    BukkitCloudServer getBukkitCloudServer(int id);

    void createNewBungeeServer(int id, int ram, String plugin);

    void removeBungeeServer(int id);

    BungeeCloudServer getBungeeCloudServer(int id);

    CloudServer getCloudServer(int id);

    List<CloudServer> getCloudserver();
}
