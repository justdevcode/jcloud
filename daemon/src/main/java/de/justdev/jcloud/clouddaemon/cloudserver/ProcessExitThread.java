package de.justdev.jcloud.clouddaemon.cloudserver;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 06.08.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class ProcessExitThread extends Thread {
    private final CloudServer cloudServer;

    public ProcessExitThread(CloudServer cloudServer) {
        this.cloudServer = cloudServer;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            log.log(Level.DEBUG, "Processexitthread was interrupted. Serverid: " + cloudServer.getId());
        }
        if (cloudServer.shouldKill) {
            log.log(Level.WARN, "Server process could not be stopped. ID:" + cloudServer.getId() + " Type:" + cloudServer.getClass());
            log.log(Level.WARN, "Killing process");
            cloudServer.kill();
            cloudServer.setProcess(null);
        }
    }
}
