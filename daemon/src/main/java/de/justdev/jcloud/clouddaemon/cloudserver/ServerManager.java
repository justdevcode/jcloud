package de.justdev.jcloud.clouddaemon.cloudserver;

import de.justdev.jcloud.clouddaemon.cloudserver.specific.BukkitCloudServer;
import de.justdev.jcloud.clouddaemon.cloudserver.specific.BungeeCloudServer;
import de.justdev.jcloud.clouddaemon.util.NoPortFoundException;
import de.justdev.jcloud.clouddaemon.util.PortFinderUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.util.ArrayList;
import java.util.List;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 31.07.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class ServerManager implements IServerManager {
    private final List<CloudServer> allServers = new ArrayList<>();

    private final List<BukkitCloudServer> bukkits = new ArrayList<>();
    private final List<BungeeCloudServer> bungees = new ArrayList<>();

    @Override
    public void creaetNewBukkitServer(int id, boolean console, boolean cracked, String craftbukkit, String world, String plugin, int ram) {
        log.log(Level.DEBUG, "Servermanager is creating a new Bukkit server with id " + id);

        //get free port
        Integer port = PortFinderUtil.getFreePort();
        if (port == null) {
            throw new NoPortFoundException("Cannot find any free port!");
        }
        //Create cloudserver control instance
        BukkitCloudServer cloudServer = new BukkitCloudServer(id, port, ram, console, cracked, craftbukkit, world, plugin);
        //start server
        cloudServer.start();
        //add server to managed server list
        addServer(cloudServer);
    }

    @Override
    public void removeBukkitServer(int id) {
        log.log(Level.DEBUG, "Servermanager is removing a Bukkit server with id " + id);
        for (int i = bukkits.size() - 1; i >= 0; i--) {
            if (bukkits.get(i).getId() == id) {
                removeServer(bukkits.get(i));
            }
        }
    }

    public BukkitCloudServer getBukkitCloudServer(int id) {
        for (BukkitCloudServer cloudServer : bukkits) {
            if (cloudServer.getId() == id) {
                return cloudServer;
            }
        }
        return null;
    }

    @Override
    public void createNewBungeeServer(int id, int ram, String plugin) {
        log.log(Level.DEBUG, "Servermanager is creating a new Bungee server with id " + id);

        //get free port
        Integer port = PortFinderUtil.getFreePort();
        if (port == null) {
            throw new NoPortFoundException("Cannot find any free port!");
        }

        BungeeCloudServer cloudServer = new BungeeCloudServer(id, port, ram, plugin);
        cloudServer.start();
        addBungee(cloudServer);
    }

    @Override
    public void removeBungeeServer(int id) {
        log.log(Level.DEBUG, "Servermanager is removing a Bungee server with id " + id);
        for (int i = bungees.size() - 1; i >= 0; i--) {
            if (bungees.get(i).getId() == id) {
                removeBungee(bungees.get(i));
            }
        }
    }

    public BungeeCloudServer getBungeeCloudServer(int id) {
        for (BungeeCloudServer cloudServer : bungees) {
            if (cloudServer.getId() == id) {
                return cloudServer;
            }
        }
        return null;
    }

    @Override
    public CloudServer getCloudServer(int id) {
        return getBukkitCloudServer(id) != null ? getBukkitCloudServer(id) : getBungeeCloudServer(id);
    }

    @Override
    public List<CloudServer> getCloudserver() {
        return allServers;
    }

    private void removeBungee(BungeeCloudServer cloudServer) {
        log.log(Level.TRACE, "Removed Bungee server from Servermanagement (" + cloudServer.getId() + ")");
        cloudServer.stop(true);
        bungees.remove(cloudServer);
        allServers.remove(cloudServer);
    }

    private void addBungee(BungeeCloudServer cloudServer) {
        log.log(Level.TRACE, "Added new Bugeecord server to Servermanagement (" + cloudServer.getId() + ")");
        bungees.add(cloudServer);
        allServers.add(cloudServer);
    }

    private void removeServer(BukkitCloudServer cloudServer) {
        log.log(Level.TRACE, "Removed Bukkit server from Servermanagement (" + cloudServer.getId() + ")");
        cloudServer.stop(true);
        bukkits.remove(cloudServer);
        allServers.remove(cloudServer);
    }

    private void addServer(BukkitCloudServer cloudServer) {
        log.log(Level.TRACE, "Added new Bukkit server to Servermanagement (" + cloudServer.getId() + ")");
        bukkits.add(cloudServer);
        allServers.add(cloudServer);
    }
}
