package de.justdev.jcloud.clouddaemon.cloudserver.specific;

import com.google.common.base.Preconditions;
import de.justdev.jcloud.clouddaemon.cloudserver.CloudServer;
import de.justdev.jcloud.clouddaemon.cloudserver.ProcessExitThread;
import de.justdev.jcloud.clouddaemon.template.InvaliedTemplateException;
import de.justdev.jcloud.clouddaemon.ui.JCloudDaemon;
import de.justdev.jcloud.clouddaemon.util.FileUtils;
import de.justdev.jcloud.protocol.packets.LogEntryPacket;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.io.File;
import java.io.IOException;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 31.07.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class BukkitCloudServer extends CloudServer {
    //fields
    private final boolean console;
    private final boolean cracked;
    private String cbs;
    private String ws;
    private String ps;

    public BukkitCloudServer(int id, int port, int ram, boolean console, boolean cracked, String cbs, String ws, String ps) {
        super(id, port, ram);
        log.log(Level.TRACE, "[BK " + getId() + "] Creating new BukkitCloudServer instance");
        this.cbs = cbs;
        this.ws = ws;
        this.ps = ps;
        this.console = console;
        this.cracked = cracked;
        this.cbs = cbs;
        this.ws = ws;
        this.ps = ps;
        setCreated(false);
    }

    @Override
    public void start() {
        log.log(Level.DEBUG, "[BK " + getId() + "] Starting Server (:" + getPort() + ")");
        //create server dir if server does no exists alreads
        if (!isCreated()) create();
        //create start string
        String[] params = new String[]{
                "java",
                "-Xmx" + getRam() + "M",
                "-Dcom.mojang.eula.agree=true",
                "-jar",
                "spigot.jar",
                "-o",
                cracked + "",
                "-p",
                getPort() + "",
                !console ? "--noconsole" : ""};

        //Create process builder
        ProcessBuilder b = new ProcessBuilder(params).directory(serverdir);
        try {
            //Start process
            process = b.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //open logreader
        openLogReader();
        pushCreated();
    }

    @Override
    public void stop(boolean remove) {
        log.log(Level.DEBUG, "[BK " + getId() + "] Stopping Server");
        //Send Masterserver removeinfo
        JCloudDaemon.getInstance().pushMaster(new LogEntryPacket("Removing Bukkit " + getId()));

        log.log(Level.TRACE, "[BK " + getId() + "] Executing stop");
        //Stop server via command
        execute("stop");

        //Close log reader
        closeLogReader();

        //wait for process exit
        try {
            log.log(Level.TRACE, "[BK " + getId() + "] wait for process to stop");
            shouldKill = true;
            processExitThread = new ProcessExitThread(this);
            processExitThread.start();
            process.waitFor();
            shouldKill = false;
            processExitThread.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            log.log(Level.TRACE, "[BK " + getId() + "] checking for still running process");
            //kill process
            kill();
            if (remove) {
                //remove directory
                log.log(Level.DEBUG, "[BK " + getId() + "] removing serverdir");
                delete();
            }
        }
    }

    private void delete() {
        log.log(Level.DEBUG, "Removing Serverdir success: + " + FileUtils.deleteDirectory(serverdir));
    }

    private void create() {
        Preconditions.checkArgument(JCloudDaemon.getInstance().getBukkitTemplateManager().containsCBTemplate(cbs), "Craftbukkit template does not exists");
        Preconditions.checkArgument(JCloudDaemon.getInstance().getBukkitTemplateManager().containsPLTemplate(ps), "Plugin template does not exists");
        Preconditions.checkArgument(JCloudDaemon.getInstance().getBukkitTemplateManager().containsWTemplate(ws), "World template does not exists");

        serverdir = new File("server/" + getId());
        try {
            JCloudDaemon.getInstance().getBukkitTemplateManager().rawServer(serverdir);
            JCloudDaemon.getInstance().getBukkitTemplateManager().equippCB(serverdir, cbs);
            JCloudDaemon.getInstance().getBukkitTemplateManager().equippPL(serverdir, ps);
            JCloudDaemon.getInstance().getBukkitTemplateManager().equippW(serverdir, ws);
        } catch (IOException e) {
            JCloudDaemon.getInstance().pushMaster(new LogEntryPacket(e.getMessage()));
            throw new InvaliedTemplateException(e.getMessage());
        }
        setCreated(true);
    }
}
