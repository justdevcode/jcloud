package de.justdev.jcloud.clouddaemon.cloudserver.specific;

import com.google.common.base.Preconditions;
import de.justdev.jcloud.clouddaemon.cloudserver.CloudServer;
import de.justdev.jcloud.clouddaemon.cloudserver.ProcessExitThread;
import de.justdev.jcloud.clouddaemon.ui.JCloudDaemon;
import de.justdev.jcloud.clouddaemon.util.FileUtils;
import de.justdev.jcloud.clouddaemon.util.PortFinderUtil;
import de.justdev.jcloud.protocol.packets.LogEntryPacket;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.io.File;
import java.io.IOException;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 31.07.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class BungeeCloudServer extends CloudServer {
    private final String plugins;
    private BungeeListener[] listeners;

    public BungeeCloudServer(int id, int port, int ram, String plugins) {
        super(id, port, ram);
        this.plugins = plugins;
        setCreated(false);
    }

    @Override
    public void start() {
        log.log(Level.DEBUG, "[BC " + getId() + "] Starting Server (:" + getPort() + ")");
        //create server dir if server does no exists alreads
        if (!isCreated()) create();
        //create start string
        String[] params = new String[]{
                "java",
                "-Xmx" + getRam() + "M",
                "-jar",
                "BungeeCord.jar"};

        //Create process builder
        ProcessBuilder b = new ProcessBuilder(params).directory(serverdir);
        try {
            //Start process
            process = b.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //open logreader
        openLogReader();
        pushCreated();
    }

    @Override
    public void stop(boolean remove) {
        log.log(Level.DEBUG, "[BC " + getId() + "] Stopping Server");
        //Send Masterserver removepacket
        JCloudDaemon.getInstance().pushMaster(new LogEntryPacket("Removing Bungee " + getId()));

        log.log(Level.TRACE, "[BC " + getId() + "] Executing stop");
        //Stop server via command
        //todo bungeecord does no recieve this command
        execute("end");

        //Close log reader
        closeLogReader();

        //wait for process exit
        try {
            log.log(Level.TRACE, "[BC " + getId() + "] wait for process to stop");
            shouldKill = true;
            processExitThread = new ProcessExitThread(this);
            processExitThread.start();
            process.waitFor();
            shouldKill = false;
            processExitThread.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            //kill process
            kill();
            if (remove) {
                //remove directory
                log.log(Level.DEBUG, "[BC " + getId() + "] removing serverdir");
                delete();
            }
        }
    }

    private void delete() {
        log.log(Level.DEBUG, "Removing Serverdir success: + " + FileUtils.deleteDirectory(serverdir));
    }

    private void create() {
        Preconditions.checkArgument(JCloudDaemon.getInstance().getBungeeTemplateManager().containsPLTemplate(this.plugins), "Plugin template does not exists");

        serverdir = new File("server/" + getId());
        JCloudDaemon.getInstance().getBungeeTemplateManager().rawServer(serverdir);
        JCloudDaemon.getInstance().getBungeeTemplateManager().equippPL(serverdir, this.plugins);
        //bungee config
        BungeeListener listener = BungeeListener.defaultListener();
        listener.setHost(PortFinderUtil.MACHINE_ADRESS + ":" + getPort());
        JCloudDaemon.getInstance().getBungeeTemplateManager().prepareConfig(serverdir, new BungeeListener[]{listener});
        //prepare config end
        setCreated(true);
    }
}
