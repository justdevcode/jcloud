package de.justdev.jcloud.clouddaemon.cloudserver.specific;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 05.08.2016.
 *
 * @author JustDev [Justus]
 */
@Setter
@Getter
@AllArgsConstructor
public class BungeeListener {
    private int maxplayers;
    private String host;
    private boolean bind_local_address;
    private boolean ping_passthrough;
    private String tab_list;
    private Map<String, String> forced_hosts;
    private int tab_size;
    private boolean force_default_server;
    private String motd;
    private boolean query_enable;
    private int query_port;
    private String[] priorities;

    public static BungeeListener defaultListener() {
        Map<String, String> forced_hosts = new HashMap<>();
        forced_hosts.put("vip", "premiumlobby");
        return new BungeeListener(-1, "localhost", true, false, "GLOBAL_PING", forced_hosts, 60, false, "Another Bungee server", false, 25577, new String[]{
                "cloud_25565"
        });
    }
}
