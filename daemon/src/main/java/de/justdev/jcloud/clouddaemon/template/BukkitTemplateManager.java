package de.justdev.jcloud.clouddaemon.template;

import com.google.common.base.Preconditions;
import de.justdev.jcloud.clouddaemon.util.FileUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 31.07.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class BukkitTemplateManager {
    private static final File BASICDIR = new File("templates/bukkit/raw/");
    private static final File CBTEMPLATEDIR = new File("templates/bukkit/cb");
    private static final File PLTEMPLATEDIR = new File("templates/bukkit/pl");
    private static final File WTEMPLATEDIR = new File("templates/bukkit/w");

    public BukkitTemplateManager() {
        System.out.println("BASEE " + BASICDIR.exists());
        System.out.println("CBE " + CBTEMPLATEDIR.exists());
        System.out.println("PLE " + PLTEMPLATEDIR.exists());
        System.out.println("WTE " + WTEMPLATEDIR.exists());
    }

    public boolean containsCBTemplate(String templateName) {
        return Arrays.stream(CBTEMPLATEDIR.listFiles()).anyMatch(f -> f.getName().equals(templateName));
    }

    public boolean containsPLTemplate(String templateName) {
        return Arrays.stream(PLTEMPLATEDIR.listFiles()).anyMatch(f -> f.getName().equals(templateName));
    }

    public boolean containsWTemplate(String templateName) {
        return Arrays.stream(WTEMPLATEDIR.listFiles()).anyMatch(f -> f.getName().equals(templateName));
    }

    //Server without cb, pl & w
    public void rawServer(File serverDir) {
        log.log(Level.TRACE, "Creating raw servertemplate in " + serverDir.getAbsolutePath());
        //check for existing server and remove if there is something left
        if (serverDir.exists()) {
            log.log(Level.WARN, "There was still some Serverstuff missing! Clean removing failed " + serverDir.getAbsolutePath());
            FileUtils.deleteDirectory(serverDir);
        }
        //create empty directory
        serverDir.mkdirs();
        //copy raw server
        FileUtils.copyFolder(BASICDIR, serverDir);
    }

    //Equipp with craftbukkit
    public void equippCB(File serverDir, String templateName) throws IOException {
        log.log(Level.TRACE, "Copieing craftbukkit type " + templateName + " to " + serverDir.getAbsolutePath());
        //get craftbukkit source
        File cb = new File(CBTEMPLATEDIR.getAbsolutePath(), templateName);
        Preconditions.checkState(cb.exists(), "CB template" + templateName + " does not exists");
        //get new file where stuff is copied to
        File ncb = new File(serverDir.getAbsolutePath(), templateName);
        //create empty jar
        ncb.createNewFile();
        //copy data
        FileUtils.copyFolder(cb, ncb);
    }

    //Equipp with plugins
    public void equippPL(File serverDir, String templateName) {
        log.log(Level.TRACE, "Copieing plugin type " + templateName + " to " + serverDir.getAbsolutePath());
        //get pl source
        File pl = new File(PLTEMPLATEDIR.getAbsolutePath() + "/" + templateName);
        Preconditions.checkState(pl.exists(), "PL template" + templateName + " does not exists");
        //copy data
        FileUtils.copyFolder(pl, serverDir);
    }

    //Equipp with world
    public void equippW(File serverDir, String templateName) {
        log.log(Level.TRACE, "Copieing craftbukkit world " + templateName + " to " + serverDir.getAbsolutePath());
        //get world source
        File w = new File(WTEMPLATEDIR.getAbsolutePath() + "/" + templateName);
        Preconditions.checkState(w.exists(), "W template" + templateName + " does not exists");
        //copy data
        FileUtils.copyFolder(w, serverDir);
    }
}
