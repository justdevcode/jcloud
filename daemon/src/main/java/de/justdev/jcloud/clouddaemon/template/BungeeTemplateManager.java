package de.justdev.jcloud.clouddaemon.template;

import com.google.common.base.Preconditions;
import de.justdev.jcloud.clouddaemon.cloudserver.specific.BungeeListener;
import de.justdev.jcloud.clouddaemon.util.FileUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Map;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 31.07.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class BungeeTemplateManager {
    private static final File BASEBUNGEETEMPLATEDIR = new File("templates/bungee/raw/");
    private static final File BUNGEEPLTEMPLATEDIR = new File("templates/bungee/plugins/");

    //Server without pl
    public void rawServer(File serverDir) {
        log.log(Level.TRACE, "Creating raw servertemplate in " + serverDir.getAbsolutePath());
        //check for existing server and remove if there is something left
        if (serverDir.exists()) {
            log.log(Level.WARN, "There was still some Serverstuff missing! Clean removing failed " + serverDir.getAbsolutePath());
            FileUtils.deleteDirectory(serverDir);
        }
        //create empty directory
        serverDir.mkdirs();
        //copy raw server
        FileUtils.copyFolder(BASEBUNGEETEMPLATEDIR, serverDir);
    }

    public boolean containsPLTemplate(String templateName) {
        return Arrays.stream(BUNGEEPLTEMPLATEDIR.listFiles()).anyMatch(f -> f.getName().equals(templateName));
    }

    //Equipp with plugins
    public void equippPL(File serverDir, String templateName) {
        log.log(Level.TRACE, "Copieing plugin type " + templateName + " to " + serverDir.getAbsolutePath());
        //get pl source
        File pl = new File(BUNGEEPLTEMPLATEDIR.getAbsolutePath() + "/" + templateName);
        Preconditions.checkState(pl.exists(), "PL template" + templateName + " does not exists");
        //copy data
        FileUtils.copyFolder(pl, serverDir);
    }

    public void prepareConfig(File serverDir, BungeeListener[] listeners) {
        log.log(Level.TRACE, "Preparing config for " + serverDir.getAbsolutePath());

        FileWriter fw = null;
        BufferedWriter bw = null;
        PrintWriter out = null;
        try {
            fw = new FileWriter(new File(serverDir.getAbsolutePath() + "/config.yml"), true);

            bw = new BufferedWriter(fw);
            out = new PrintWriter(bw);
            out.println("listeners:");
            for (BungeeListener bungeeListener : listeners) {
                out.println("- max_players: " + bungeeListener.getMaxplayers());
                out.println("  host: " + bungeeListener.getHost());
                out.println("  bind_local_address: " + bungeeListener.isBind_local_address());
                out.println("  ping_passthrough: " + bungeeListener.isPing_passthrough());
                out.println("  tab_list: " + bungeeListener.getTab_list());
                out.println("  priorities: ");
                for (String priority : bungeeListener.getPriorities()) {
                    out.println("  - " + priority);
                }
                out.println("  forced_hosts: ");
                for (Map.Entry<String, String> entry : bungeeListener.getForced_hosts().entrySet()) {
                    out.println("    " + entry.getKey() + ": " + entry.getValue());
                }
                out.println("  tab_size: " + bungeeListener.getTab_size());
                out.println("  force_default_server: " + bungeeListener.isForce_default_server());
                out.println("  motd: " + bungeeListener.getMotd());
                out.println("  query_enabled: " + bungeeListener.isQuery_enable());
                out.println("  query_port: " + bungeeListener.getQuery_port());
                out.println("# ========== [ listener end ] ==========");
            }
            out.println("player_limit: -1");
            out.println("online_mode: true");
            out.println("log_commands: false");
            out.println("disabled_commands:");
            out.println("- disabledcommandhere");
            out.println("connection_throttle: 4000");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null)
                out.close();
            if (bw != null)
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            if (fw != null)
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}
