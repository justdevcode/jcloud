package de.justdev.jcloud.clouddaemon.util;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 09.08.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class OSValidator {

    private final String OS = System.getProperty("os.name").toLowerCase();

    public boolean validate() {
        if (!isUnix()) {
            System.out.println("OS usages are not supported in " + OS + " yet!");
            return false;
        } else {
            log.log(Level.DEBUG, "Your OS is support, we can access the OS usages.");
            return true;
        }
    }

    private boolean isUnix() {
        return (OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0);
    }
}