package de.justdev.jcloud.clouddaemon.util;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Consumer;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 04.08.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class StreamGobbler extends Thread {
    private final InputStream is;
    private Consumer<String> consumer;
    private boolean stop = false;

    public StreamGobbler(InputStream is) {
        this.is = is;
    }

    protected void setConsumer(Consumer<String> consumer) {
        this.consumer = consumer;
    }

    @Override
    public void run() {
        log.log(Level.TRACE, "Starting new Stream gobbler");
        try {
            //create
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while (!stop && (line = br.readLine()) != null) {
                consumer.accept(line);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void stopReading() {
        log.log(Level.TRACE, "Stopping new Stream gobbler");
        interrupt();
        stop = true;
    }
}