package de.justdev.jcloud.event;

import de.justdev.jcloud.protocol.packets.Packet;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 09.07.2016.
 *
 * @author JustDev [Justus]
 */
public class EventBus {
    private final Map<Class<? extends Packet>, Map<IListener, Method>> listeners = new ConcurrentHashMap<>();

    /**
     * Add a new event class
     *
     * @param event Class object of the event
     */
    public void registerEvent(Class<? extends Packet> event) {
        listeners.putIfAbsent(event, new ConcurrentHashMap<>());
    }

    /**
     * Dispatch an event globally
     *
     * @param event Instance of the event
     * @return Returns whether the event was dispatched successfully
     */
    public boolean dispatchEvent(Event event) {
        final Map<IListener, Method> methods = listeners.get(event.getClass());
        if (methods == null || methods.size() == 0)
            return false;
        try {
            for (Map.Entry<IListener, Method> entry : methods.entrySet())
                entry.getValue().invoke(entry.getKey(), event);
            return true;
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Register new event listener
     *
     * @param listener listener to register
     */
    public void registerListener(IListener listener) {
        for (Method method : listener.getClass().getMethods()) {
            if (!method.isAnnotationPresent(EventHandler.class))
                continue;
            final Class<?>[] parameters = method.getParameterTypes();
            if (parameters.length != 1)
                continue;
            listeners.keySet().forEach(listen -> {
                if (parameters[0].isAssignableFrom(listen))
                    listeners.get(listen).putIfAbsent(listener, method);
            });
        }
    }
}
