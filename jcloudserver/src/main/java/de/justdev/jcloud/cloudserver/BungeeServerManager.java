package de.justdev.jcloud.cloudserver;

import com.google.common.base.Preconditions;
import de.justdev.jcloud.cloudserver.api.JCloud;
import de.justdev.jcloud.cloudserver.cloudserver.BungeeCloudServer;
import de.justdev.jcloud.cloudserver.cloudserver.DaemonServer;
import de.justdev.jcloud.cloudserver.cloudserver.GameCloudServer;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.util.ArrayList;
import java.util.List;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 09.08.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class BungeeServerManager {
    private static final ServerManager serverManager = JCloud.getInstance().getServerManager();

    final List<BungeeCloudServer> bungees = new ArrayList<>();
    private final List<BungeeCloudServer> pending = new ArrayList<>();

    public BungeeServerManager startUp() {
        createBungee("plugins");
        return this;
    }

    public void createBungee(String plugins) {
        //try to start new Server
        DaemonServer startingServer = serverManager.getDaemonServerManager().getPreperedDaemon(false);
        int id = serverManager.getNextId();
        BungeeCloudServer cloudServer = new BungeeCloudServer(id);

        if (startingServer == null) {
            serverManager.quene(cloudServer);
            log.log(Level.ERROR, "Trying to start new bungee server. There are no resources left!");
            return;
        }

        listBungee(cloudServer);
        startingServer.createBungee(id, "plugins", 512);
    }

    public void listBungee(BungeeCloudServer cloudServer) {
        pending.add(cloudServer);
        serverManager.allServers.put(cloudServer.getId(), cloudServer);
    }

    public void state(int id, short playerCount, String state) {
        BungeeCloudServer cloudServer = getBungeeCloudServer(id);
        Preconditions.checkNotNull(cloudServer, "Cloudserver cannot be null");

        cloudServer.setPlayerCount(playerCount);
        cloudServer.setState(state);
    }

    public BungeeCloudServer getBungeeCloudServer(int id) {
        for (BungeeCloudServer bungee : bungees) {
            if (bungee.getId() == id) {
                return bungee;
            }
        }
        return null;
    }

    public void unlistBugee(BungeeCloudServer cloudServer) {
        bungees.remove(cloudServer);
        serverManager.allServers.remove(cloudServer);
    }

    public void unregisterGameServer(GameCloudServer cloudServer) {
        bungees.stream().filter(BungeeCloudServer::isConnected).forEach(bungee -> bungee.unregisterServer(cloudServer.getId()));
    }

    public boolean markOnline(int id) {
        for (BungeeCloudServer bungee : bungees) {
            if (bungee.getId() == id) {
                pending.remove(bungee);
                bungees.add(bungee);
                bungee.initServer(serverManager.getGameServerManager().bukkits);
                addBungeeDns(bungee);
                return true;
            }
        }
        return false;
    }

    void addBungeeDns(BungeeCloudServer bungee) {
        log.log(Level.INFO, "Adding dns entry " + bungee.getHost() + " : " + bungee.getPort() + " is not implemented yet!");
    }
}
