package de.justdev.jcloud.cloudserver;

import de.justdev.jcloud.cloudserver.api.JCloud;
import de.justdev.jcloud.cloudserver.bootstrap.ChannelTracker;
import de.justdev.jcloud.cloudserver.cloudserver.DaemonServer;
import io.netty.channel.Channel;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 09.08.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class DaemonServerManager {
    private static final ServerManager serverManager = JCloud.getInstance().getServerManager();

    private final List<DaemonServer> daemons = new ArrayList<>();

    public void unlistDaemon(DaemonServer cloudServer) {
        daemons.remove(cloudServer);
        serverManager.allServers.remove(cloudServer);
    }

    public DaemonServer getDaemonServer(int id) {
        for (DaemonServer daemon : daemons) {
            if (daemon.getId() == id) {
                return daemon;
            }
        }
        return null;
    }

    public int addDaemonServer(Channel channel) {
        int id = serverManager.getNextId();
        channel.attr(ChannelTracker.SERVER_ID).set(id);
        listDaemon(new DaemonServer(id, channel));
        return id;
    }

    public void listDaemon(DaemonServer cloudServer) {
        daemons.add(cloudServer);
        serverManager.allServers.put(cloudServer.getId(), cloudServer);
    }

    public DaemonServer getPreperedDaemon(boolean useSave) {
        DaemonServer chosenServer = null;
        for (DaemonServer daemon : daemons) {
            if (!daemon.isActive() || (!useSave || daemon.isSave())) {
                continue;
            }

            if (daemon.getFreeRam() < (ServerManager.MIN_FREE_RAM_ON_DEMON + ServerManager.FREE_RAM_TO_START_SERVER)) {
                continue;
            }

            if (chosenServer != null && daemon.getCpuUsagePC() + daemon.getRamUsagePC() / 2 < chosenServer.getCpuUsagePC() + chosenServer.getRamUsagePC() / 2) {
                chosenServer = daemon;
            }
        }
        if (chosenServer == null && !useSave) getPreperedDaemon(true);
        return chosenServer;
    }

}
