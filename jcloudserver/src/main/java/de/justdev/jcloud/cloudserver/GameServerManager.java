package de.justdev.jcloud.cloudserver;

import com.google.common.base.Preconditions;
import de.justdev.jcloud.cloudserver.api.JCloud;
import de.justdev.jcloud.cloudserver.cloudserver.BungeeCloudServer;
import de.justdev.jcloud.cloudserver.cloudserver.DaemonServer;
import de.justdev.jcloud.cloudserver.cloudserver.GameCloudServer;
import de.justdev.jcloud.cloudserver.logic.GameType;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.util.ArrayList;
import java.util.List;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 09.08.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class GameServerManager {
    private static final ServerManager serverManager = JCloud.getInstance().getServerManager();
    protected final List<GameCloudServer> bukkits = new ArrayList<>();
    private final List<GameCloudServer> pending = new ArrayList<>();
    private List<GameType> gameTypes = new ArrayList<>();

    public GameServerManager(List<GameType> gameTypes) {
        this.gameTypes = gameTypes;
    }

    public GameServerManager startUp() {
        for (GameType type : gameTypes) {
            int startNServers = type.getMinStockServer();
            for (int i = 0; i < startNServers; i++) {
                createBukkit(true, true, type.getCb(), type.getWorld(), type.getPlugins(),
                        type.getRamPerServer());
            }
        }
        return this;
    }

    public void createBukkit(boolean console, boolean cracked, String craftbukkit, String world, String plugins, int ram) {
        //try to start new Server
        DaemonServer startingServer = serverManager.getDaemonServerManager().getPreperedDaemon(false);
        int id = serverManager.getNextId();
        GameCloudServer cloudServer = new GameCloudServer(id);

        if (startingServer == null) {
            serverManager.quene(new GameCloudServer(id));
            log.log(Level.ERROR, "Trying to start new bukkit server. There are no resources left!");
            return;
        }

        listBukkit(cloudServer);
        startingServer.createBukkit(id, console, cracked, craftbukkit, world, plugins, ram);
    }

    public void listBukkit(GameCloudServer cloudServer) {
        pending.add(cloudServer);
        serverManager.allServers.put(cloudServer.getId(), cloudServer);
    }

    public void state(int id, short playerCount, String state) {
        GameCloudServer cloudServer = getGameCloudServer(id);
        Preconditions.checkNotNull(cloudServer, "CloudServer cannot be null");

        cloudServer.setPlayerCount(playerCount);
        if (cloudServer.getState().equalsIgnoreCase(state)) {
            return;
        }
        if (cloudServer.getState().equalsIgnoreCase("lobby") && !cloudServer.getState().equalsIgnoreCase("premium")) {
            if (!state.equalsIgnoreCase("lobby") && !state.equalsIgnoreCase("premium")) {
                //check if we should start new server
                if (cloudServer.getGameType().getRunningStockServers() < cloudServer.getGameType().getShouldStockServer()) {
                    //check if we should warn
                    if (cloudServer.getGameType().getRunningStockServers() < cloudServer.getGameType().getMinStockServer()) {
                        log.log(Level.WARN, "The stock server limit of " + cloudServer.getGameType() + " has been reached!");
                    }

                    createBukkit(true, true, cloudServer.getGameType().getCb(),
                            cloudServer.getGameType().getWorld(), cloudServer.getGameType().getPlugins(), cloudServer.getGameType().getRamPerServer());

                }
            }
        }
        cloudServer.setState(state);
    }

    public GameCloudServer getGameCloudServer(int id) {
        for (GameCloudServer game : bukkits) {
            if (game.getId() == id) {
                return game;
            }
        }
        return null;
    }

    public void unlistBukkit(GameCloudServer cloudServer) {
        serverManager.getBungeeServerManager().unregisterGameServer(cloudServer);
        bukkits.remove(cloudServer);
        serverManager.allServers.remove(cloudServer);
    }

    public boolean markOnline(int id) {
        for (GameCloudServer bukkit : bukkits) {
            if (bukkit.getId() == id) {
                pending.remove(bukkit);
                bukkits.add(bukkit);
                serverManager.getBungeeServerManager().bungees.stream().filter(BungeeCloudServer::isConnected).forEach(bungeeCloudServer -> bungeeCloudServer.registerServer(bukkit.getId(), bukkit.getHost(), bukkit.getPort()));
                return true;
            }
        }
        return false;
    }

}
