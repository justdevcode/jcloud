package de.justdev.jcloud.cloudserver;

import de.justdev.jcloud.cloudserver.cloudserver.BungeeCloudServer;
import de.justdev.jcloud.cloudserver.cloudserver.CloudServer;
import de.justdev.jcloud.cloudserver.cloudserver.DaemonServer;
import de.justdev.jcloud.cloudserver.cloudserver.GameCloudServer;
import de.justdev.jcloud.cloudserver.logic.GameType;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 07.08.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class ServerManager {
    static final int MIN_FREE_RAM_ON_DEMON = 2048;
    static final int FREE_RAM_TO_START_SERVER = 800;
    final Map<Integer, CloudServer> allServers = new HashMap<>();
    private final List<CloudServer> quenedServer = new ArrayList<>();
    private DaemonServerManager daemonServerManager;
    private BungeeServerManager bungeeServerManager;
    private GameServerManager gameServerManager;
    private Thread crawlerThread;
    private int lastId = 0;

    public void init(List<GameType> gameTypes) {
        daemonServerManager = new DaemonServerManager();
        bungeeServerManager = new BungeeServerManager().startUp();
        gameServerManager = new GameServerManager(gameTypes).startUp();
    }

    public CloudServer getServer(int id) {
        CloudServer returnServer = getDaemonServerManager().getDaemonServer(id);
        if (returnServer == null) returnServer = getBungeeServerManager().getBungeeCloudServer(id);
        if (returnServer == null) returnServer = getGameServerManager().getGameCloudServer(id);
        return returnServer;
    }

    public DaemonServerManager getDaemonServerManager() {
        return daemonServerManager;
    }

    public BungeeServerManager getBungeeServerManager() {
        return bungeeServerManager;
    }

    public GameServerManager getGameServerManager() {
        return gameServerManager;
    }

    public void registerServer(int id, String host, int port) {
        for (BungeeCloudServer bungee : getBungeeServerManager().bungees) {
            if (bungee.getId() == id) {
                bungee.setHost(host);
                bungee.setPort(port);
                return;
            }
        }
        for (GameCloudServer bukkit : getGameServerManager().bukkits) {
            if (bukkit.getId() == id) {
                bukkit.setHost(host);
                bukkit.setPort(port);
                return;
            }
        }
        log.log(Level.ERROR, "Server " + id + " register, but there is no server with id " + id + " in our system!");
    }

    public void markOnline(int id) {
        if (!bungeeServerManager.markOnline(id))
            gameServerManager.markOnline(id);
    }

    public int getNextId() {
        if (lastId >= 2147483647 || lastId < 0) {
            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                if (!allServers.containsKey(i)) {
                    return i;
                }
            }
        }
        return lastId++;
    }

    public void quene(CloudServer cloudServer) {
        synchronized (this) {
            quenedServer.add(cloudServer);

            if (crawlerThread == null) {
                crawlerThread = new Thread() {
                    @Override
                    public void run() {
                        if (quenedServer.size() == 0) {
                            log.log(Level.DEBUG, "Finishing quene crawler thread! quene list worked out");
                            return;
                        }
                        CloudServer targetServer = quenedServer.get(0);
                        if (targetServer instanceof GameCloudServer || targetServer instanceof BungeeCloudServer) {
                            DaemonServer daemon = getDaemonServerManager().getPreperedDaemon(false);

                            if (daemon != null) {
                                if (targetServer instanceof GameCloudServer) {
                                    log.log(Level.DEBUG, "Crawler is creating Gameserver");
                                    daemon.createBukkit(targetServer.getId(),
                                            true, true, ((GameCloudServer) targetServer).getGameType().getCb(),
                                            ((GameCloudServer) targetServer).getGameType().getWorld(),
                                            ((GameCloudServer) targetServer).getGameType().getPlugins(),
                                            targetServer.getRam());
                                    quenedServer.remove(targetServer);
                                } else if (targetServer instanceof BungeeCloudServer) {
                                    log.log(Level.DEBUG, "Crawler is creating Bungeeserver");
                                    daemon.createBungee(targetServer.getId(),
                                            targetServer.getTemplate(), targetServer.getRam());
                                    quenedServer.remove(targetServer);
                                }
                            }
                        } else {
                            log.log(Level.WARN, "Invalied servertype in quene list!");
                        }

                        try {
                            Thread.sleep(1000 * 3);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        log.log(Level.DEBUG, "Rerunning crawler");
                        run();
                    }
                };
                log.log(Level.DEBUG, "Starting quene crawler");
                crawlerThread.start();
            }
        }
    }

}
