package de.justdev.jcloud.cloudserver.bootstrap;

import de.justdev.jcloud.cloudserver.GameTypeLoader;
import de.justdev.jcloud.cloudserver.ServerManager;
import de.justdev.jcloud.cloudserver.api.JCloud;
import de.justdev.jcloud.cloudserver.authentication.BungeeAuthenticationHandler;
import de.justdev.jcloud.cloudserver.authentication.DaemonAuthenticationHandler;
import de.justdev.jcloud.cloudserver.authentication.IAuthenticationHandler;
import de.justdev.jcloud.event.EventBus;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 09.08.2016.
 *
 * @author JustDev [Justus]
 */
public class JCloudManager extends JCloud {
    //todo implement counters
    private int playerCount = 0;
    private int bungeeCount = 0;
    private int bukkitCount = 0;
    private int daemonCount = 0;

    private final EventBus eventBus = new EventBus();
    private final IAuthenticationHandler daemonAuthenticationHandler = new DaemonAuthenticationHandler();
    private final IAuthenticationHandler bungeeAuthentitcationHandler = new BungeeAuthenticationHandler();
    private ServerManager serverManager;

    @Override
    public EventBus getEventManager() {
        return eventBus;
    }

    @Override
    public IAuthenticationHandler getDaemonAuthenticationHandler() {
        return daemonAuthenticationHandler;
    }

    @Override
    public IAuthenticationHandler getBungeeAuthenticationHandler() {
        return bungeeAuthentitcationHandler;
    }

    @Override
    public ServerManager getServerManager() {
        return serverManager;
    }

    public void init() {
        serverManager = new ServerManager();
        serverManager.init(new GameTypeLoader().getGameTypes());
    }

    @Override
    public int getPlayerCount() {
        return playerCount;
    }

    @Override
    public int getBungeeCount() {
        return bungeeCount;
    }

    @Override
    public int getBukkitCount() {
        return bukkitCount;
    }

    @Override
    public int getDaemonCount() {
        return daemonCount;
    }
}
