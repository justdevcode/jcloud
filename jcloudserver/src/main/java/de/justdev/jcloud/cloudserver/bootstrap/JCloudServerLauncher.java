package de.justdev.jcloud.cloudserver.bootstrap;


import de.justdev.jcloud.cloudserver.api.JCloud;
import de.justdev.jcloud.cloudserver.bootstrap.io.handler.AuthenticationHandler;
import de.justdev.jcloud.console.CommandHandling;
import de.justdev.jcloud.console.Console;
import de.justdev.jcloud.protocol.io.handler.LengthBasedDecoder;
import de.justdev.jcloud.protocol.io.handler.LengthBasedEncoder;
import de.justdev.jcloud.protocol.io.handler.PacketDecoder;
import de.justdev.jcloud.protocol.io.handler.PacketEncoder;
import de.justdev.jcloud.protocol.packets.ShutdownPacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.CreateBungeePacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.CreateServerPacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.RemoveBungeePacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.RemoveServerPacket;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

@Log4j2
public class JCloudServerLauncher {
    //generall
    /* CONSTANCES */
    public static final int PORT = 22220;

    //console
    public final Console console = new Console(new CommandHandling());

    //daemon connections
    public final ChannelGroup daemonConnections = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    //local netty stuff
    private final NioEventLoopGroup bossGroup;
    private final NioEventLoopGroup workerGroup;
    private ChannelFuture hostChannelFuture;

    private ServerBootstrap bootstrap;

    public JCloudServerLauncher() {
        JCloud.setInstance(new JCloudManager());
        JCloud.getInstance().init();
        //init event loop groups
        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();

        try {
            //bootstrap
            bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup);
            bootstrap.channel(NioServerSocketChannel.class);

            //prepare pipeline on channel init
            bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    log.log(Level.INFO, "Neue Client connectet!");
                    //prepare masterChannel pipeline
                    ChannelPipeline pipeline = socketChannel.pipeline();
                    pipeline.addLast("framedecoder", new LengthBasedDecoder());
                    pipeline.addLast("frameencoder", new LengthBasedEncoder());
                    pipeline.addLast("encoder", new PacketEncoder());
                    pipeline.addLast("decoder", new PacketDecoder());
                    pipeline.addLast("authhandler", new AuthenticationHandler());

                    //add channel to channel list
                    daemonConnections.add(socketChannel);
                }
            });

            bootstrap.option(ChannelOption.SO_BACKLOG, 50);
            bootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);

            //bind server to port
            hostChannelFuture = bootstrap.bind(PORT).sync();
            log.log(Level.INFO, "Server auf Port " + PORT + " gestartet!");

            //register basic command
            testCommands();
            //Start console listening
            console.read();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (hostChannelFuture != null) {
                try {
                    hostChannelFuture.channel().close().sync();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //shutdown event loop groups
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }

    }

    private void testCommands() {
        //Register command create
        console.getCommandHandling().registerExecutor("create", args -> {
            if (args.length != 7) {
                System.out.println("Bitte verwende \"create id console cracked spigot world pl ram\"");
                return;
            }
            for (Channel c : daemonConnections) {
                c.writeAndFlush(new CreateServerPacket(Integer.parseInt(args[0]), Boolean.parseBoolean(args[1]), Boolean.parseBoolean(args[2]), args[3], args[4], args[5], Integer.parseInt(args[6])));
            }
            return;
        });

        //register command remove
        console.getCommandHandling().registerExecutor("remove", args -> {
            if (args.length != 1) {
                System.out.println("Bitte verwende \"remove id\"");
                return;
            }
            for (Channel c : daemonConnections) {
                c.writeAndFlush(new RemoveServerPacket(Integer.parseInt(args[0])));
            }
            return;
        });

        //Register command newbungee
        console.getCommandHandling().registerExecutor("newbungee", args -> {
            if (args.length != 3) {
                System.out.println("Bitte verwende \"newbungee id plugins ram\"");
                return;
            }
            for (Channel c : daemonConnections) {
                c.writeAndFlush(new CreateBungeePacket(Integer.parseInt(args[0]), Integer.parseInt(args[2]), args[1]));
                System.out.println("send");
            }
            return;
        });

        //register command remove
        console.getCommandHandling().registerExecutor("delbungee", args -> {
            if (args.length != 1) {
                System.out.println("Bitte verwende \"delbungee id\"");
                return;
            }
            for (Channel c : daemonConnections) {
                c.writeAndFlush(new RemoveBungeePacket(Integer.parseInt(args[0])));
                System.out.println("send");
            }
            return;
        });

        //register command exit
        console.getCommandHandling().registerExecutor("exit", args -> {
            shutdown();
            return;
        });

    }

    private void shutdown() {
        //close connections
        log.log(Level.INFO, "Shutting down " + daemonConnections.size() + " deamons");
        while (daemonConnections.iterator().hasNext()) {
            try {
                daemonConnections.writeAndFlush(new ShutdownPacket("Masterserver is shutting down"));
                daemonConnections.iterator().next().close().sync();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //shutdown host
        try {
            //close host channel
            hostChannelFuture.channel().close().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            //shutdown event loop groups
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
        //stop console
        console.stopReading();
    }

    public static void main(String[] args) {
        new JCloudServerLauncher();
    }
}
