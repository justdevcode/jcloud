package de.justdev.jcloud.cloudserver.bootstrap.io.handler;

import de.justdev.jcloud.cloudserver.api.JCloud;
import de.justdev.jcloud.cloudserver.bootstrap.ChannelTracker;
import de.justdev.jcloud.protocol.packets.RegisterPacket;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 05.08.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class AuthenticationHandler extends de.justdev.jcloud.protocol.io.handler.AuthenticationHandler {
    @Override
    public boolean valiedAuth(RegisterPacket registerPacket, ChannelHandlerContext ctx) {
        if (registerPacket.getType().equalsIgnoreCase("daemon")) {
            return JCloud.getInstance().getDaemonAuthenticationHandler().accept(ctx, registerPacket);
        } else if (registerPacket.getType().equalsIgnoreCase("bungee")) {
            return JCloud.getInstance().getBungeeAuthenticationHandler().accept(ctx, registerPacket);
        } else {
            log.log(Level.WARN, "Registering with unknown Type!");
        }
        return false;
    }

    @Override
    public void onSuccess(RegisterPacket packet, ChannelHandlerContext ctx) {
        //add essential packethandler when auth is completed successfully
        ctx.pipeline().addAfter("decoder", "handler", new PacketHandler());
        if (packet.getType().equalsIgnoreCase("daemon")) {
            ctx.attr(ChannelTracker.SERVER_TYPE).set("daemon");
            int id = JCloud.getInstance().getServerManager().getDaemonServerManager().addDaemonServer(ctx.channel());
            ctx.attr(ChannelTracker.SERVER_ID).set(id);
        } else if (packet.getType().equalsIgnoreCase("bungee")) {
            ctx.attr(ChannelTracker.SERVER_TYPE).set("bungee");
            JCloud.getInstance().getServerManager().getBungeeServerManager().markOnline(packet.getId());
            ctx.attr(ChannelTracker.SERVER_ID).set(packet.getId());
        } else {
            log.log(Level.WARN, "Registering with unknown Type!");
        }
    }
}
