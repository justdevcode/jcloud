package de.justdev.jcloud.cloudserver.bootstrap.io.handler;

import de.justdev.jcloud.cloudserver.api.JCloud;
import de.justdev.jcloud.cloudserver.bootstrap.ChannelTracker;
import de.justdev.jcloud.protocol.packets.LogEntryPacket;
import de.justdev.jcloud.protocol.packets.Packet;
import de.justdev.jcloud.protocol.packets.PacketTypes;
import de.justdev.jcloud.protocol.packets.bungeelayerprotocol.BungeeStatePacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.BungeeCreatedPacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.DaemonInfoPacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.ForwardEntryPacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.HandshakePacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.ServerCreatedPacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.ServerRegisterPacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.StatePacket;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 09.07.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class PacketHandler extends de.justdev.jcloud.protocol.io.handler.PacketHandler {

    @Override
    protected void onChannelRead(ChannelHandlerContext ctx, Packet packet, PacketTypes type) {
        switch (type) {
            /*
             * TOPLAYER
             */
            case TOPLAYER_HANDSHAKE: {
                HandshakePacket packet1 = ((HandshakePacket) packet);
                log.log(Level.INFO, "Daemon Handshake: Name: " + packet1.getName() + " Version: " + packet1.getVersion() + " CPU: " + packet1.getAllowCpu() + " RAM: " + packet1.getAllowRam());
                break;
            }

            case TOPLAYER_INFO: {
                DaemonInfoPacket packet1 = ((DaemonInfoPacket) packet);
                if (ctx.attr(ChannelTracker.SERVER_ID).get() == null) {
                    log.log(Level.WARN, "ID from daemonserver got lost! recieving packet, without knowing the id " +
                            "from the server the packet is from!");
                    return;
                }
                JCloud.getInstance().getServerManager().getDaemonServerManager().getDaemonServer(ctx.attr(ChannelTracker.SERVER_ID).get()).prepareUsageData(packet1);
                break;
            }

            case TOPLAYER_BUNGEECREATED: {
                BungeeCreatedPacket packet1 = ((BungeeCreatedPacket) packet);
                JCloud.getInstance().getServerManager().registerServer(packet1.getId(), packet1.getHost(), packet1.getPort());
                break;
            }

            case TOPLAYER_SERVERCREATED: {
                ServerCreatedPacket packet1 = ((ServerCreatedPacket) packet);
                JCloud.getInstance().getServerManager().registerServer(packet1.getId(), packet1.getHost(), packet1.getPort());
                log.log(Level.INFO, "New Server! ID:" + packet1.getId() + " on " + packet1.getHost() + ":" + packet1.getPort());
                break;
            }

            case TOPLAYER_REGISTERSERVER: {
                ServerRegisterPacket packet1 = ((ServerRegisterPacket) packet);
                JCloud.getInstance().getServerManager().markOnline(packet1.getId());
                break;
            }

            case TOPLAYER_DISCONNECTSERVER: {
                //todo gameserver disconnected
                break;
            }

            case TOPLAYER_STATESERVER: {
                StatePacket packet1 = ((StatePacket) packet);
                JCloud.getInstance().getServerManager().getGameServerManager().state(packet1.getId(), packet1.getPlayerCount(), packet1.getState());
                break;

            }

            case TOPLAYER_LOG: {
                ForwardEntryPacket packet1 = ((ForwardEntryPacket) packet);
                log.log(Level.INFO, "[" + ctx.attr(ChannelTracker.SERVER_ID).get() + "] forwarding for " + packet1.getId() + ": " + packet1.getLogEntry());
                break;
            }

            case BUNGEELAYER_STATE: {
                BungeeStatePacket packet1 = ((BungeeStatePacket) packet);
                JCloud.getInstance().getServerManager().getBungeeServerManager().state(ctx.attr(ChannelTracker.SERVER_ID).get(), packet1.getPlayerCount(), packet1.getState());
                break;
            }

            /*
             * ALAYER
             */

            case REGISTER_AUTH: {
                log.log(Level.INFO, "Client tries to authenticate twice");
                break;
            }

            case DISCONNECT: {
                log.log(Level.INFO, "Master cannot get disconnected!");
                break;
            }

            case SHUTDOWN: {
                log.log(Level.INFO, "Recieving shutdown packet! Master should not recieve this packet!");
                break;
            }

            case LOG: {
                LogEntryPacket packet1 = ((LogEntryPacket) packet);
                log.log(Level.INFO, "[" + ctx.attr(ChannelTracker.SERVER_ID) + "] (" + ctx.attr(ChannelTracker.SERVER_TYPE).get() + " ): " + packet1.getLogEntry());
                break;
            }

            default: {
                log.log(Level.WARN, "Unhandled packet: " + packet.getClass());
                break;
            }
        }
    }
}
