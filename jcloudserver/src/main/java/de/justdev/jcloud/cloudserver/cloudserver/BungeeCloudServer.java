package de.justdev.jcloud.cloudserver.cloudserver;

import de.justdev.jcloud.protocol.packets.bungeelayerprotocol.AddServerToBungeePacket;
import de.justdev.jcloud.protocol.packets.bungeelayerprotocol.RemoveServerFromBungeePacket;
import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 07.08.2016.
 *
 * @author JustDev [Justus]
 */
@Getter
public class BungeeCloudServer extends CloudServer {
    private Channel connection;
    @Setter
    private short playerCount;
    @Setter
    private String state;

    @Setter
    private String host;
    @Setter
    private int port = -1;

    public BungeeCloudServer(int id) {
        super(id);
    }

    public void initServer(List<GameCloudServer> cloudServerList) {
        cloudServerList.forEach(s -> {
            if (s.isOnline())
                connection.writeAndFlush(new AddServerToBungeePacket(String.valueOf(s.getId()), s.getHost(), s.getPort()));
        });
    }

    public void registerServer(int id, String host, int port) {
        connection.writeAndFlush(new AddServerToBungeePacket(String.valueOf(id), host, port));
    }

    public void unregisterServer(int id) {
        connection.writeAndFlush(new RemoveServerFromBungeePacket(String.valueOf(id)));
    }

    public boolean isConnected() {
        return connection != null && connection.isOpen() && connection.isActive();
    }
}
