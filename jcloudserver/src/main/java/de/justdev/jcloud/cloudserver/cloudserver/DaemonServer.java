package de.justdev.jcloud.cloudserver.cloudserver;

import de.justdev.jcloud.protocol.packets.toplayerprotocol.CreateBungeePacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.CreateServerPacket;
import de.justdev.jcloud.protocol.packets.toplayerprotocol.DaemonInfoPacket;
import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 07.08.2016.
 *
 * @author JustDev [Justus]
 */
@Getter
@Setter
public class DaemonServer extends CloudServer {
    private final Channel connection;

    private boolean active; // if not active do not use
    private boolean save; //save ressources, fill as last

    private int maxRam;
    private int ramUsed;
    private float cpuUsagePC;

    private int servers;
    private int bungees;
    private int maxServers;
    private int getMaxServers;

    public DaemonServer(int id, Channel channel) {
        super(id);
        this.connection = channel;
    }

    public boolean isConnected() {
        return connection != null && connection.isOpen() && connection.isActive();
    }

    public int getFreeRam() {
        return maxRam - ramUsed;
    }

    public float getRamUsagePC() {
        return (float) maxRam / ramUsed * 100;
    }

    public void createBukkit(int id, boolean console, boolean cracked, String craftbukkit, String world, String plugins, int ram) {
        CreateServerPacket packet = new CreateServerPacket(id, console, cracked, craftbukkit, world, plugins, ram);
        connection.writeAndFlush(packet);
    }

    public void createBungee(int id, String template, int ram) {
        CreateBungeePacket packet = new CreateBungeePacket(id, ram, template);
        connection.writeAndFlush(packet);
    }

    public void prepareUsageData(DaemonInfoPacket packet1) {
        //todo
    }
}
