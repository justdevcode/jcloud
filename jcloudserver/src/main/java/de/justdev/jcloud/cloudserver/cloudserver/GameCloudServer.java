package de.justdev.jcloud.cloudserver.cloudserver;

import de.justdev.jcloud.cloudserver.logic.GameType;
import lombok.Getter;
import lombok.Setter;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 07.08.2016.
 *
 * @author JustDev [Justus]
 */
@Getter
public class GameCloudServer extends CloudServer {
    private final long createdDate;
    private GameType gameType;
    @Setter
    private String host;
    @Setter
    private int port = -1;
    @Setter
    private short playerCount;

    public GameCloudServer(int id) {
        super(id);
        this.createdDate = System.currentTimeMillis();
    }

    public boolean isOnline() {
        return port != -1;
    }
}
