package de.justdev.jcloud.maintenance.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 07.08.2016.
 *
 * @author JustDev [Justus]
 */
public class JVMDumper {

    public static void dumpJVM(String fileName) {
        File dumpFile = new File(fileName);
        FileWriter dumpWriter = null;

        try {
            if (!dumpFile.exists()) {
                dumpFile.createNewFile();
            }
            dumpWriter = new FileWriter(dumpFile);
            dumpWriter.write("Created: " + System.currentTimeMillis());

            dumpWriter.write("\nJVM Security manager\n");
            dumpWriter.write("\t" + System.getSecurityManager() != null && System.getSecurityManager().getClass() != null ? System.getSecurityManager().getClass().getName() : "none");

            dumpWriter.write("\nJVM ram usage\n");
            dumpWriter.write("\tUsed Memory:"
                    + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1048576 + "mb");
            dumpWriter.write("\n\tFree Memory:"
                    + Runtime.getRuntime().freeMemory() / 1048576 + "mb");
            dumpWriter.write("\n\tTotal Memory:" + Runtime.getRuntime().totalMemory() / 1048576 + "mb");
            dumpWriter.write("\n\tMax Memory:" + Runtime.getRuntime().maxMemory() / 1048576 + "mb");

            dumpWriter.write("\nJVM start arguments\n\t");
            RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
            List<String> arguments = runtimeMxBean.getInputArguments();
            for (String argument : arguments) {
                dumpWriter.write(argument + "; ");
            }

            dumpWriter.write("\nJVM system properties\n\t");
            Properties props = System.getProperties();
            props.list(new PrintWriter(dumpFile));

            dumpWriter.write("\nJVM enviroment\n");
            Map<String, String> env = System.getenv();
            for (Map.Entry<String, String> enviromentPropertie : env.entrySet()) {
                dumpWriter.write("\t" + enviromentPropertie.getKey() + " -> " + enviromentPropertie.getValue() + "\n");
            }
            dumpWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (dumpWriter != null) {
                try {
                    dumpWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
