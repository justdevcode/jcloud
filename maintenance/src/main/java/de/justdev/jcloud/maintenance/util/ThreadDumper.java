package de.justdev.jcloud.maintenance.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 07.08.2016.
 *
 * @author JustDev [Justus]
 */
public class ThreadDumper {

    public static void dumpThread(String fileName) {
        File dumpFile = new File(fileName);
        BufferedWriter dumpWriter = null;

        try {
            if (!dumpFile.exists()) {
                dumpFile.createNewFile();
            }
            dumpWriter = new BufferedWriter(new FileWriter(dumpFile));

            final ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
            final ThreadInfo[] threadInfos = threadMXBean.getThreadInfo(threadMXBean.getAllThreadIds(), 100);

            for (ThreadInfo threadInfo : threadInfos) {
                dumpWriter.append('"');
                dumpWriter.append(threadInfo.getThreadName());
                dumpWriter.append("\" ");
                final Thread.State state = threadInfo.getThreadState();
                dumpWriter.append("\n   java.lang.Thread.State: ");
                dumpWriter.append(state.name());
                final StackTraceElement[] stackTraceElements = threadInfo.getStackTrace();
                for (final StackTraceElement stackTraceElement : stackTraceElements) {
                    dumpWriter.append("\n        at ");
                    dumpWriter.append(stackTraceElement.toString());
                }
                dumpWriter.append("\n\n");
            }
            dumpWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (dumpWriter != null) {
                try {
                    dumpWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
