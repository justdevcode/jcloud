package de.justdev.jcloud.protocol.io.handler;

import com.google.common.base.Preconditions;
import de.justdev.jcloud.protocol.packets.Packet;
import de.justdev.jcloud.protocol.packets.PacketTypes;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.List;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 09.07.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class PacketDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf in, List<Object> out) throws Exception {
        //check for empty bytebuf
        if (in.readableBytes() == 0) {
            log.log(Level.WARN, "Recieving empty bytebuf");
            return;
        }

        //Read packetid
        int id = in.readShort();

        //Create Inputstream to easily read things from bytebuf
        ByteBufInputStream stream = new ByteBufInputStream(in);

        //find constructor without parameters
        Constructor emptyConstructor = PacketTypes.getPacketTypeById(id).getClazz().getConstructors()[0];
        int counter = 1;
        while (emptyConstructor.getParameterCount() != 0 && counter < PacketTypes.getPacketTypeById(id).getClazz().getConstructors().length) {
            if (PacketTypes.getPacketTypeById(id).getClazz().getConstructors()[counter].getParameterCount() == 0) {
                emptyConstructor = PacketTypes.getPacketTypeById(id).getClazz().getConstructors()[counter++];
            }
        }
        //check if non parameter consturcor was found
        Preconditions.checkState(emptyConstructor.getParameterCount() == 0, "Packet constructur requires "
                + emptyConstructor.getParameterCount() + " Arguments! 0 needed");
        //Instanciate packet
        Packet packet = (Packet) emptyConstructor.newInstance();

        //Write packet fields in order of bytestreamfields
        for (Field f : packet.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            if (f.getType().isAssignableFrom(byte[].class)) {
                short size = stream.readShort();
                byte[] array = new byte[size];
                for (int i = 0; i < size; i++) {
                    array[i] = stream.readByte();
                }
                f.set(packet, array);
            } else if (f.getType().isAssignableFrom(short[].class)) {
                short size = stream.readShort();
                short[] array = new short[size];
                for (int i = 0; i < size; i++) {
                    array[i] = stream.readShort();
                }
                f.set(packet, array);
            } else if (f.getType().isAssignableFrom(int[].class)) {
                short size = stream.readShort();
                int[] array = new int[size];
                for (int i = 0; i < size; i++) {
                    array[i] = stream.readInt();
                }
                f.set(packet, array);
            } else if (f.getType().isAssignableFrom(long[].class)) {
                short size = stream.readShort();
                long[] array = new long[size];
                for (int i = 0; i < size; i++) {
                    array[i] = stream.readLong();
                }
                f.set(packet, array);
            } else if (f.getType().isAssignableFrom(float[].class)) {
                short size = stream.readShort();
                float[] array = new float[size];
                for (int i = 0; i < size; i++) {
                    array[i] = stream.readFloat();
                }
                f.set(packet, array);
            } else if (f.getType().isAssignableFrom(double[].class)) {
                short size = stream.readShort();
                double[] array = new double[size];
                for (int i = 0; i < size; i++) {
                    array[i] = stream.readDouble();
                }
                f.set(packet, array);
            } else if (f.getType().isAssignableFrom(char[].class)) {
                short size = stream.readShort();
                char[] array = new char[size];
                for (int i = 0; i < size; i++) {
                    array[i] = stream.readChar();
                }
                f.set(packet, array);
            } else if (f.getType().isAssignableFrom(String[].class)) {
                short size = stream.readShort();
                String[] array = new String[size];
                for (int i = 0; i < size; i++) {
                    array[i] = stream.readUTF();
                }
                f.set(packet, array);
            } else if (f.getType().isAssignableFrom(boolean[].class)) {
                short size = stream.readShort();
                boolean[] array = new boolean[size];
                for (int i = 0; i < size; i++) {
                    array[i] = stream.readBoolean();
                }
                f.set(packet, array);
            } else if (f.getType().equals(byte.class)) {
                f.setByte(packet, stream.readByte());
            } else if (f.getType().isAssignableFrom(short.class)) {
                f.setShort(packet, stream.readShort());
            } else if (f.getType().isAssignableFrom(int.class)) {
                f.setInt(packet, stream.readInt());
            } else if (f.getType().isAssignableFrom(long.class)) {
                f.setLong(packet, stream.readLong());
            } else if (f.getType().isAssignableFrom(float.class)) {
                f.setFloat(packet, stream.readFloat());
            } else if (f.getType().isAssignableFrom(double.class)) {
                f.setDouble(packet, stream.readDouble());
            } else if (f.getType().isAssignableFrom(char.class)) {
                f.setChar(packet, stream.readChar());
            } else if (f.getType().equals(String.class)) {
                f.set(packet, stream.readUTF());
            } else if (f.getType().equals(boolean.class)) {
                f.setBoolean(packet, stream.readBoolean());
            } else {
                throw new IllegalArgumentException("Recieving packet (" + packet.getClass().getName() + ") " +
                        "with unknown Type to decode (" + f.getType().getName() + ")");
            }
        }

        //send packet to outgoing handlers
        out.add(packet);
    }
}
