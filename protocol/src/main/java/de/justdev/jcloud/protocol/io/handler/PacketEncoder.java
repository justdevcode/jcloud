package de.justdev.jcloud.protocol.io.handler;

import de.justdev.jcloud.protocol.packets.Packet;
import de.justdev.jcloud.protocol.packets.PacketTypes;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.lang.reflect.Field;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 09.07.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public class PacketEncoder extends MessageToByteEncoder<Packet> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Packet packet, ByteBuf byteBuf) throws Exception {
        //Write packet id
        byteBuf.writeShort(PacketTypes.getPacketTypeByClass(packet.getClass()).getId());

        //Create outputstream for bytebuf to write things easily
        ByteBufOutputStream stream = new ByteBufOutputStream(byteBuf);

        PacketTypes type = PacketTypes.getPacketTypeById(packet.getPacketID());
        log.log(Level.DEBUG, ">>>>>>>>>>>>>>>>[ OUTGOING PACKET (" + type.getId() + ") ]>>>>>>>>>>>>>>>>");
        log.log(Level.DEBUG, "Sending packet ID:" + type.getId() + " Type:" + type.getClazz().getSimpleName());
        log.log(Level.DEBUG, "Packet class: " + type.getClazz().getName());
        log.log(Level.DEBUG, "");
        try {
            //Write packet data in order of packet fields
            for (Field f : packet.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                log.log(Level.DEBUG, f.getName() + " (" + f.getType() + "): " + f.get(packet));
                if (f.getType().isAssignableFrom(byte[].class)) {
                    stream.writeShort(((byte[]) f.get(packet)).length);
                    for (byte c : (byte[]) f.get(packet)) {
                        stream.writeByte(c);
                    }
                } else if (f.getType().isAssignableFrom(short[].class)) {
                    stream.writeShort(((short[]) f.get(packet)).length);
                    for (short c : (short[]) f.get(packet)) {
                        stream.writeShort(c);
                    }
                } else if (f.getType().isAssignableFrom(int[].class)) {
                    stream.writeShort(((int[]) f.get(packet)).length);
                    for (int c : (int[]) f.get(packet)) {
                        stream.writeInt(c);
                    }
                } else if (f.getType().isAssignableFrom(long[].class)) {
                    stream.writeShort(((long[]) f.get(packet)).length);
                    for (long c : (long[]) f.get(packet)) {
                        stream.writeLong(c);
                    }
                } else if (f.getType().isAssignableFrom(float[].class)) {
                    stream.writeShort(((float[]) f.get(packet)).length);
                    for (float c : (float[]) f.get(packet)) {
                        stream.writeFloat(c);
                    }
                } else if (f.getType().isAssignableFrom(double[].class)) {
                    stream.writeShort(((double[]) f.get(packet)).length);
                    for (double c : (double[]) f.get(packet)) {
                        stream.writeDouble(c);
                    }
                } else if (f.getType().isAssignableFrom(char[].class)) {
                    stream.writeShort(((char[]) f.get(packet)).length);
                    for (char c : (char[]) f.get(packet)) {
                        stream.writeChar(c);
                    }
                } else if (f.getType().isAssignableFrom(String[].class)) {
                    stream.writeShort(((String[]) f.get(packet)).length);
                    for (String c : (String[]) f.get(packet)) {
                        stream.writeUTF(c);
                    }
                } else if (f.getType().isAssignableFrom(boolean[].class)) {
                    stream.writeShort(((boolean[]) f.get(packet)).length);
                    for (boolean c : (boolean[]) f.get(packet)) {
                        stream.writeBoolean(c);
                    }
                } else if (f.getType().isAssignableFrom(byte.class)) {
                    stream.writeByte(f.getByte(packet));
                } else if (f.getType().isAssignableFrom(short.class)) {
                    stream.writeShort(f.getShort(packet));
                } else if (f.getType().isAssignableFrom(int.class)) {
                    stream.writeInt(f.getInt(packet));
                } else if (f.getType().isAssignableFrom(long.class)) {
                    stream.writeLong(f.getLong(packet));
                } else if (f.getType().isAssignableFrom(float.class)) {
                    stream.writeFloat(f.getFloat(packet));
                } else if (f.getType().isAssignableFrom(double.class)) {
                    stream.writeDouble(f.getDouble(packet));
                } else if (f.getType().isAssignableFrom(char.class)) {
                    stream.writeChar(f.getChar(packet));
                } else if (f.getType().isAssignableFrom(String.class)) {
                    stream.writeUTF(((String) f.get(packet)));
                } else if (f.getType().isAssignableFrom(boolean.class)) {
                    stream.writeBoolean(f.getBoolean(packet));
                } else {
                    throw new IllegalStateException("Try sending Packet (" + packet.getClass().getName() + ") " +
                            "with unknown Type to encode (" + f.getType().getName() + ")");
                }
            }
            log.log(Level.DEBUG, "");
            log.log(Level.DEBUG, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
