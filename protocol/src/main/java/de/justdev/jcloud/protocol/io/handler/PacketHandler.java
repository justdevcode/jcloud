package de.justdev.jcloud.protocol.io.handler;

import de.justdev.jcloud.protocol.packets.NonPacketRecievingException;
import de.justdev.jcloud.protocol.packets.Packet;
import de.justdev.jcloud.protocol.packets.PacketTypes;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.lang.reflect.Field;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 05.08.2016.
 *
 * @author JustDev [Justus]
 */
@Log4j2
public abstract class PacketHandler extends ChannelHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (!(msg instanceof Packet)) {
            log.log(Level.WARN, "Recieving invalied Object " + msg.getClass() + " at PacketHandler");
            throw new NonPacketRecievingException("Reciving non Packet @PacketHandler (" + msg.getClass() + ")");
        }
        Packet packet = ((Packet) msg);
        PacketTypes type = PacketTypes.getPacketTypeById(packet.getPacketID());
        
        /* ============================================= DEBUG MSG ============================================= */
        log.log(Level.DEBUG, "<<<<<<<<<<<<<<<<[ INCOMMING PACKET (" + type.getId() + ") ]<<<<<<<<<<<<<<<<");
        log.log(Level.DEBUG, "Recieving packet ID:" + type.getId() + " Type:" + type.getClazz().getSimpleName());
        log.log(Level.DEBUG, "Packet class: " + type.getClazz().getName());
        log.log(Level.DEBUG, "");
        for (Field f : packet.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            log.log(Level.DEBUG, f.getName() + " (" + f.getType() + "): " + f.get(packet));
        }
        log.log(Level.DEBUG, "");
        log.log(Level.DEBUG, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        /* ============================================= DEBUG MSG ============================================= */

        onChannelRead(ctx, packet, type);
    }

    protected abstract void onChannelRead(ChannelHandlerContext ctx, Packet packet, PacketTypes type);
}
