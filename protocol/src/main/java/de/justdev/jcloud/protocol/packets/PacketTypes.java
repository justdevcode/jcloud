package de.justdev.jcloud.protocol.packets;

import de.justdev.jcloud.protocol.packets.toplayerprotocol.DaemonInfoPacket;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 09.07.2016.
 *
 * @author JustDev [Justus]
 */
public enum PacketTypes {
    TOPLAYER_HANDSHAKE((byte) 0x00, de.justdev.jcloud.protocol.packets.toplayerprotocol.HandshakePacket.class, false, false),
    TOPLAYER_INFO((byte) 0x01, DaemonInfoPacket.class, false, false),
    TOPLAYER_CREATEBUNGEE((byte) 0x02, de.justdev.jcloud.protocol.packets.toplayerprotocol.CreateBungeePacket.class, false, false),
    TOPLAYER_BUNGEECREATED((byte) 0x03, de.justdev.jcloud.protocol.packets.toplayerprotocol.BungeeCreatedPacket.class, false, false),
    TOPLAYER_REMOVEBUNGEE((byte) 0x04, de.justdev.jcloud.protocol.packets.toplayerprotocol.RemoveBungeePacket.class, false, false),
    TOPLAYER_CREATESERVER((byte) 0x05, de.justdev.jcloud.protocol.packets.toplayerprotocol.CreateServerPacket.class, false, false),
    TOPLAYER_SERVERCREATED((byte) 0x06, de.justdev.jcloud.protocol.packets.toplayerprotocol.ServerCreatedPacket.class, false, false),
    TOPLAYER_REMOVESERVER((byte) 0x07, de.justdev.jcloud.protocol.packets.toplayerprotocol.RemoveServerPacket.class, false, false),
    TOPLAYER_LOG((byte) 0x08, de.justdev.jcloud.protocol.packets.toplayerprotocol.ForwardEntryPacket.class, false, false),
    TOPLAYER_DISCONNECTSERVER((byte) 0x09, de.justdev.jcloud.protocol.packets.toplayerprotocol.ServerDisconnectedPacket.class, false, false),
    TOPLAYER_REGISTERSERVER((byte) 0x10, de.justdev.jcloud.protocol.packets.toplayerprotocol.ServerRegisterPacket.class, false, false),
    TOPLAYER_STATESERVER((byte) 0x11, de.justdev.jcloud.protocol.packets.toplayerprotocol.StatePacket.class, false, false),
    TOPLAYER_COMMAND((byte) 0x12, de.justdev.jcloud.protocol.packets.toplayerprotocol.CommandPacket.class, false, false),
    TOPLAYER_SHUTDOWNSERVER((byte) 0x13, de.justdev.jcloud.protocol.packets.toplayerprotocol.ShutdownServerPacket.class, false, false),

    BUNGEELAYER_ADDSERVERTOBUNGEE((byte) 0x14, de.justdev.jcloud.protocol.packets.bungeelayerprotocol.AddServerToBungeePacket.class, false, false),
    BUNGEELAYER_REMOVESERVERFROMBUNGEE((byte) 0x15, de.justdev.jcloud.protocol.packets.bungeelayerprotocol.RemoveServerFromBungeePacket.class, false, false),
    BUNGEELAYER_STATE((byte) 0x16, de.justdev.jcloud.protocol.packets.bungeelayerprotocol.BungeeStatePacket.class, false, false),

    LOWERPLAYER_STATESERVER((byte) 0x17, de.justdev.jcloud.protocol.packets.lowerlayerprotocol.StatePacket.class, false, false),

    REGISTER_AUTH((byte) 0x18, de.justdev.jcloud.protocol.packets.RegisterPacket.class, false, false),
    LOG((byte) 0x19, LogEntryPacket.class, false, false),
    COMMAND((byte) 0x20, de.justdev.jcloud.protocol.packets.CommandPacket.class, false, false),
    DISCONNECT((byte) 0x21, de.justdev.jcloud.protocol.packets.YouWereDisconnectedPacket.class, false, false),
    SHUTDOWN((byte) 0x22, de.justdev.jcloud.protocol.packets.ShutdownPacket.class, false, false);

    private final short id;
    private final Class clazz;
    private final boolean eventFireing;
    private final boolean cancellableeventFireing;

    PacketTypes(short id, Class clazz, boolean eventFireing, boolean eventHandling) {
        this.id = id;
        this.clazz = clazz;
        this.eventFireing = eventFireing;
        this.cancellableeventFireing = eventHandling;
    }

    public static PacketTypes getPacketTypeById(int id) {
        for (int i = 0; i < values().length; i++) {
            if (values()[i].getId() == id) {
                return values()[i];
            }
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public static PacketTypes getPacketTypeByClass(Class clazz) {
        for (int i = 0; i < values().length; i++) {
            if (values()[i].getClazz() == clazz) {
                return values()[i];
            }
        }
        return null;
    }

    public Class getClazz() {
        return clazz;
    }

    public boolean isEventFireing() {
        return eventFireing;
    }

    public boolean isCancellableeventFireing() {
        return cancellableeventFireing;
    }
}
