/*
 * Classes in this package are for the protocol between a subserver and
 * the clouddaemon
 */
package de.justdev.jcloud.protocol.packets.lowerlayerprotocol;