package de.justdev.jcloud.util;

import com.sun.management.OperatingSystemMXBean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.Map;

/**
 * This SOFTWARE PRODUCT is provided by THE PROVIDER "as is" and "with all faults."
 * THE PROVIDER makes no representations or warranties of any kind concerning the
 * safety, suitability, lack of viruses, inaccuracies, typographical errors, or other
 * harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use
 * of any software, and you are solely responsible for determining whether this SOFTWARE
 * PRODUCT is compatible with your equipment and other software installed on your
 * equipment. You are also solely responsible for the protection of your equipment and
 * backup of your data, and THE PROVIDER will not be liable for any damages you may
 * suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT
 * <p>
 * Created by Justus on 09.08.2016.
 *
 * @author JustDev [Justus]
 */
public class Watcher {
    private static final OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);

    /**
     * memoryUsage gives us data about memory usage (RAM and SWAP)
     */
    public static Map<String, Integer> getUsageData() {
        BufferedReader freeReader = null;
        String freeLine;
        String[] freeChunkedLine;
        HashMap<String, Integer> usageData = new HashMap<>();

        try {
            Runtime runtime = Runtime.getRuntime();
            Process freeProcess = runtime.exec("free -k"); // We measure memory in kilobytes to obtain a greater granularity

            freeReader = new BufferedReader(new InputStreamReader(freeProcess.getInputStream()));

            // We discard the first line
            freeReader.readLine();

            freeLine = freeReader.readLine();
            if (freeLine == null) {
                throw new Exception("free didn't work well");
            }
            freeChunkedLine = freeLine.split("\\s+");

            usageData.put("memory_total", Integer.parseInt(freeChunkedLine[1]));

            freeLine = freeReader.readLine();
            if (freeLine == null) {
                throw new Exception("free didn't work well");
            }
            freeChunkedLine = freeLine.split("\\s+");

            usageData.put("memory_used", Integer.parseInt(freeChunkedLine[2]));

            freeLine = freeReader.readLine();
            if (freeLine == null) {
                throw new Exception("free didn't work well");
            }
            freeChunkedLine = freeLine.split("\\s+");

            usageData.put("swap_total", Integer.parseInt(freeChunkedLine[1]));
            usageData.put("swap_used", Integer.parseInt(freeChunkedLine[2]));


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (freeReader != null) try {
                freeReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // What % CPU load this current JVM is taking, percent = value / 10
        usageData.put("jvm_cpu_usage", (int) ((double) (operatingSystemMXBean.getProcessCpuLoad() * 1000d)));
        System.out.println("procesload " + String.valueOf(operatingSystemMXBean.getProcessCpuLoad()));
        // What % load the overall system is at, percent = value / 10
        usageData.put("cpu_used", (int) ((double) (operatingSystemMXBean.getSystemCpuLoad() * 1000d)));
        System.out.println("cpuload " + String.valueOf(operatingSystemMXBean.getSystemCpuLoad()));
        //0.9 = 90%
        //0.9 -> 900
        //

        return usageData;
    }
}
